#version 330 core

uniform vec2 iResolution;
uniform float iTime;
uniform int iFrame;
in vec2 fragCoord; 
out vec4 fragColor;

uniform sampler2D bufferTexture;

#define M_PI 3.1415925585

////data structures for ray tracing
struct camera{
    vec3 origin;
    vec3 horizontal;
    vec3 vertical;
    vec3 LowerLeftCorner;
};

struct ray{
    vec3 ori;
    vec3 dir;
};

struct sphere{
    vec3 ori;			////sphere center
    float r;			////sphere radius
    vec3 color;			////sphere color
};

struct light {
   vec3 position;		////point light position
   vec3 color;			////point light color
};
    
struct hit{
    float t;			////parameter in the ray function
    vec3 p;				////intersection point
    vec3 normal;		////normal on the intersection point
    vec3 color;			////color of the intersecting object
};

struct grav{
    vec3 p;
    float radius; // theta = 2 * Schwarzchild Radius / r
};

struct gal{
    vec3 p; //center of "galaxy" plane which is normal to z axis
};

//////////// Random functions ///////////
float g_seed = 0.;

uint base_hash(uvec2 p) {
    p = 1103515245U*((p >> 1U)^(p.yx));
    uint h32 = 1103515245U*((p.x)^(p.y>>3U));
    return h32^(h32 >> 16);
}

void init_rand(in vec2 frag_coord, in float time) {
    g_seed = float(base_hash(floatBitsToUint(frag_coord)))/float(0xffffffffU)+time;
}

vec2 rand2(inout float seed) {
    uint n = base_hash(floatBitsToUint(vec2(seed+=.1,seed+=.1)));
    uvec2 rz = uvec2(n, n*48271U);
    return vec2(rz.xy & uvec2(0x7fffffffU))/float(0x7fffffff);
}
/////////////////////////////////////////

const float minT = 0.001;
const float maxT = 1e8;
const int numberOfSampling = 50;

////if no hit is detected, return dummyHit
const hit dummyHit = hit(-1.0, vec3(0), vec3(0), vec3(0));

////calculate the ray for a given uv coordinate
ray getRay(camera c, vec2 uv)
{
    return ray(c.origin, c.LowerLeftCorner + uv.x * c.horizontal + uv.y * c.vertical - c.origin);
}

ray deflectRay(ray r, grav g, float time)
{
    //testing 
    
    float Schwarzchild = g.radius;
    vec3 rayposition = r.dir * time + r.ori;

    //return ray(rayposition, r.dir);


    vec3 v = g.p - rayposition;

    if(length(v) < Schwarzchild) {
        return ray(rayposition, vec3(0)); //ray is within event horizon
    }

    float theta = 2.0 * Schwarzchild / (length(v));

    vec3 v_ = normalize(v);

    vec3 dir_ = normalize(r.dir);

    float max_theta = acos(dot(v_, dir_)); // maximum theta to move by, cannot deflect ray more than radius to grav source;

    vec3 d = cross(cross(dir_, v_), v_); //make vector 90 degrees from direction  of ray towards the grav source
    float d_len = length(d);

    if(d_len < 0.001) { // make sure they arent parallel/antiparallel
        return ray(rayposition, dir_);
    }

    vec3 d_ = normalize(d);

    //theta = max(theta, max_theta);

    return ray(rayposition, cos(theta)*dir_ + sin(theta)*d_);

}

float hitLensePlane(ray r, grav g)
{
    float t = -1.0;

    t = (g.p.z - r.ori.z)/(r.dir.z);

    return t;
}



//inspiration taken from https://www.shadertoy.com/view/XlfGRj
//kaliset fractal http://www.fractalforums.com/3d-fractal-generation/kaliset-3d-fractal-used-as-coloring-and-bump-mapping-for-de-systems/
vec3 starsColor(float u, float v) 
{
    vec2 uv = vec2(u,v);
	

	float val= 0.0;
    vec3 p;
	
    //for certain "depths" calculate stars using kaliset fractal
	for (int i = 0; i < 100; i++) {
		p = vec3(uv, float(i)*.07);
		p += vec3(.2,  .6, -.73-cos(iTime*.001)*.1);

		for (int i = 0; i < 8; i++){ //kaliset fractal
            p = abs(p) / dot(p,p) - .7;
        }

		float p2 = dot(p,p)*.0015;
		val += p2 ;
	}
	vec3 col = vec3(val,  val,  val);
    return col;


}

//inspiration taken from https://www.shadertoy.com/view/XdsSRS
vec3 galaxyColor(float u, float v)
{
    vec2 uv = vec2(u, v);
    float len = length(uv.xy);
	
    float t = .1*iTime;

	float time = t  +  (5.+sin(t))*.11 / (len+.07); // spiraling

	uv *= mat2(cos(time), sin(time), -sin(time), cos(time));// rotation
    
    vec3 col = starsColor(uv.x, uv.y);

    //add blue tint to spiral;

    col = col.x * vec3(135.,206.,250.)/255.;

    //add bright light in middle of galaxy
    
    float rho = len/.1;
    float light_density = exp(-rho*rho);
    
    col += light_density * vec3(255.,255.,204.)/255.;

    col *= smoothstep(.6, .0, len);

    return col;
    

}

hit hitStarsPlane(ray r, gal g)
{
    float t = -1.0;

    t = (g.p.z - r.ori.z)/(r.dir.z);

    //find u v in plane

    vec3 point = r.dir*t + r.ori;
    float u = point.x/40.0;
    float v = point.y/40.0;

    vec3 col = starsColor(u,v);

    col = min(pow(col.x, 6), 1.0) * vec3(255.,255.,204.)/255.; 
    return hit(t, r.dir*t + r.ori, vec3(0.0,0.0,1.0), col);
}

hit hitGalaxyPlane(ray r, gal g)
{
    float t = -1.0;

    t = (g.p.z - r.ori.z)/(r.dir.z);

    //find u v in plane

    vec3 point = r.dir*t + r.ori;
    float u = (point.x - g.p.x)/30.0;
    float v = (point.y-g.p.y)/10.0;

    vec3 col = galaxyColor(u,v);
    return hit(t, r.dir*t + r.ori, vec3(0.0,0.0,1.0), col* col);
}




hit findHit(ray r, gal[2] galaxy, grav g) 
{
	hit h = dummyHit;
    ray working_ray = r;

    float plane_hit_t = hitLensePlane(r, g);

    if(plane_hit_t > 0.0) {
        working_ray = deflectRay(r, g, plane_hit_t);

        if(all(equal(working_ray.dir, vec3(0.0)))){
             return dummyHit;
        }
        h = hitGalaxyPlane(working_ray, galaxy[1]);

        hit h2 = hitStarsPlane(working_ray, galaxy[0]);

        h.color = h.color + h2.color;
    }

	return h;
}

vec3 color(ray r, gal[2] galaxy, grav g)
{
    vec3 col = vec3(0);
    hit h = findHit(r, galaxy, g);
    return h.color;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;
    camera c = camera(vec3(0,0,0), vec3(5, 0, 0), vec3(0, 5, 0), vec3(-2.5, -2.5, -1));

    gal g[2];
    g[0] = gal(vec3(0,0,-11)); // stars plane
    g[1] = gal(vec3(10,10, -12)); // galaxy plane


    grav blackhole;
    blackhole = grav(vec3(mod(iFrame/10.0, 30.0)-12.0,mod(iFrame/10.0, 30.0)-12.0,-5), .5); // move blackhole across screen on line y = x
    vec3 resultCol = vec3(0);
    vec3 resultCol2 = vec3(0);

    // Here I use i to get differnet seeds for each run
    init_rand(fragCoord, iTime);
    vec2 random = rand2(g_seed);

    ray r = getRay(c, uv);
    resultCol += color(r, g,blackhole);

    
    
	// Output to screen
    fragColor = vec4(resultCol, 1.); //so i can do animation

    //fragColor = vec4((resultCol + resultCol2)/2.0, 1.);
}

void main() {
	mainImage(fragColor, fragCoord);
}
